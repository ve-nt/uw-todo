const std = @import("std");
const Address = std.net.Address;
const r = @import("routez");
const Uuid = u32;
const Allocator = std.mem.Allocator;

var alloc: Allocator = undefined;

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer std.debug.assert(!gpa.deinit());
    alloc = gpa.allocator();

    todo_items = std.AutoHashMap(u32, TodoItem).init(alloc);
    defer {
        var iter = todo_items.iterator();
        while (iter.next()) |item| alloc.free(item.value_ptr.content);
        todo_items.deinit();
    }

    var server = r.Server.init(
        alloc,
        .{},
        .{
            r.post("/todo", postTodo),
            r.get("/todo/{uuid}", getTodo),
        },
    );
    var addr = try Address.parseIp("127.0.0.1", 3030);

    std.log.info("listening...\n", .{});
    try server.listen(addr);
}

const TodoItem = struct {
    uuid: ?Uuid = null,
    content: []const u8,

    // Caller owns returned memory.
    pub fn serialize(this: TodoItem, allocator: Allocator) ![]u8 {
        var res_buf = std.ArrayList(u8).init(allocator);
        const res_writer = res_buf.writer();
        try std.json.stringify(this, .{}, res_writer);
        return res_buf.toOwnedSlice();
    }

    pub fn deserialize(json: []const u8) !TodoItem {
        var tokens = std.json.TokenStream.init(json);
        const parse_type = TodoItem;
        const parse_opts = .{
            .allocator = alloc,
            .ignore_unknown_fields = true,
        };
        var todo_item = try std.json.parse(parse_type, &tokens, parse_opts);
        defer std.json.parseFree(parse_type, todo_item, parse_opts);

        return TodoItem{
            .uuid = nextUuid(),
            .content = try alloc.dupe(u8, todo_item.content),
        };
    }

    var current_uuid: u32 = 0;
    fn nextUuid() u32 {
        const uuid = current_uuid;
        current_uuid += 1;
        return uuid;
    }
};

var todo_items: std.AutoHashMap(u32, TodoItem) = undefined;
var todo_items_mutex = std.Thread.Mutex{};

fn postTodo(req: r.Request, res: r.Response) !void {
    todo_items_mutex.lock();
    defer todo_items_mutex.unlock();

    const todo_item = try TodoItem.deserialize(req.body);
    try todo_items.put(todo_item.uuid.?, todo_item);

    const serialized = try todo_item.serialize(alloc);
    defer alloc.free(serialized);
    try res.write(serialized);
}

fn getTodo(_: r.Request, res: r.Response, args: *const struct {
    uuid: u32,
}) !void {
    todo_items_mutex.lock();
    defer todo_items_mutex.unlock();

    const todo_item = todo_items.get(args.uuid) orelse return error.NoSuchTodo;

    const serialized = try todo_item.serialize(alloc);
    defer alloc.free(serialized);
    try res.write(serialized);
}
